package main

import (
	"fmt"
	"log"

	u "gitlab.com/zimbabe/golessons/gitlab.com/zimbabe/go/hw-3"
)

func main() {

	var pkgString u.PackedString

	for {
		fmt.Print("Veeeeeeeeedi: ")

		_, err := fmt.Scanf("%s", &pkgString)
		if err != nil {
			log.Println(err)
		} else {
			fmt.Println("Stroka: ", pkgString.Unpack())
			fmt.Println("")
		}
	}
}
